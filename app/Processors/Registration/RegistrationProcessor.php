<?php

namespace App\Processors\Registration;

use App\Processors\Registration\Pages\AccountDetailsPage;
use App\Processors\Registration\Pages\FirstPage;
use App\Processors\Registration\Pages\RenderablePageInterface;
use App\Processors\Registration\Pages\SecondPage;
use App\Processors\Registration\Pages\SuccessPage;
use Illuminate\Http\Request;

/**
 * Class RegistrationProcessor
 * @package App\Processors\Registration
 */
class RegistrationProcessor
{
    /**
     * @var RenderablePageInterface
     */
    protected $pageResolver;
    /**
     * @var array
     */
    protected $steps;

    /**
     * RegistrationProcessor constructor.
     * @param int $step
     */
    public function __construct(int $step)
    {
        $this->steps = [
            FirstPage::PAGE_TYPE => new FirstPage(),
            SecondPage::PAGE_TYPE => new SecondPage(),
            AccountDetailsPage::PAGE_TYPE => new AccountDetailsPage(),
            SuccessPage::PAGE_TYPE => new SuccessPage()
        ];

        $stepClass = $this->steps[$step] ?? null;
        abort_unless($stepClass, 404);
        $this->setPageResolver($stepClass);
    }

    /**
     * @return string
     */
    public function render(): string
    {
        return $this->pageResolver->render();
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function save(Request $request): bool
    {
        return $this->pageResolver->save($request);
    }

    /**
     * @return RenderablePageInterface|null
     */
    public function getNextPageResolver(): ?RenderablePageInterface
    {
        return $this->pageResolver->getNextPageResolver();
    }

    /**
     * @return string|null
     */
    public function getPageUrl(): ?string
    {
        return $this->pageResolver->getPageUrl();
    }

    /**
     * @param RenderablePageInterface $pageResolver
     */
    protected function setPageResolver(RenderablePageInterface $pageResolver): void
    {
        $this->pageResolver = $pageResolver;
    }
}
