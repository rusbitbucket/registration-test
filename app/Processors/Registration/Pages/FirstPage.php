<?php

namespace App\Processors\Registration\Pages;

use App\Models\User;
use Illuminate\Http\Request;

class FirstPage implements RenderablePageInterface
{
    const PAGE_TYPE = 1;

    /**
     * @return string
     */
    public function render(): string
    {
        session()->put('lastStep', self::PAGE_TYPE);
        return view('registration.pages.first', [
            'firstname' => auth()->user()->firstname ?? '',
            'lastname' => auth()->user()->lastname ?? '',
            'telephone' => auth()->user()->telephone ?? '',
        ])->render();
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function save(Request $request): bool
    {
        $request->validate([
            'firstname' => 'required|string|min:1|max:100',
            'lastname' => 'required|string|min:1|max:100',
            'telephone' => 'required|string|min:1|max:40',
        ]);

        if (!auth()->check()) {
            $user = User::create($request->all());
            auth()->login($user);
        } else {
            auth()->user()->update($request->all());
        }

        return true;
    }

    /**
     * @return RenderablePageInterface|null
     */
    public function getNextPageResolver(): ?RenderablePageInterface
    {
        return new SecondPage();
    }

    /**
     * @return string|null
     */
    public function getPageUrl(): ?string
    {
        return route('page.save', ['step' => self:: PAGE_TYPE]);
    }
}
