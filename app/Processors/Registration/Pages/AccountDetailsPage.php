<?php

namespace App\Processors\Registration\Pages;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class AccountDetailsPage implements RenderablePageInterface
{
    const PAGE_TYPE = 3;

    /**
     * @return string
     */
    public function render(): string
    {
        session()->put('lastStep', self::PAGE_TYPE);
        return view('registration.pages.account', [
            'accountOwner' => auth()->user()->account_owner ?? '',
            'iban' => auth()->user()->iban ?? '',
        ])->render();
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function save(Request $request): bool
    {
        if (!auth()->check()) {
           redirect()->to((new FirstPage())->getPageUrl())->send();
        }

        $request->validate([
            'account_owner' => 'required|string|min:1|max:100',
            'iban' => 'required|string|min:1|max:50',
        ]);

        try {
            $response = Http::post(config('app.wunder-mobility-endpoint'), [
                'customerId' => auth()->user()->id,
                'iban' => $request->iban,
                'owner' => $request->account_owner,
            ]);
        } catch (Exception $exception) {
            Log::info($exception->getMessage());
            return false;
        }

        if ($response->failed()) {
            Log::info($response->body());
            return false;
        }

        auth()->user()->update(
            array_merge($request->all(), ['data' => $response->json()])
        );

        session()->remove('lastStep');

        return true;
    }

    /**
     * @return RenderablePageInterface|null
     */
    public function getNextPageResolver(): ?RenderablePageInterface
    {
        return new SuccessPage();
    }

    /**
     * @return string|null
     */
    public function getPageUrl(): ?string
    {
        return route('page.save', ['step' => self:: PAGE_TYPE]);
    }
}
