<?php

namespace App\Processors\Registration\Pages;

use Illuminate\Http\Request;

class SuccessPage implements RenderablePageInterface
{
    const PAGE_TYPE = 4;

    /**
     * @return string
     */
    public function render(): string
    {
        if (!auth()->check() || !auth()->user()->data) {
            redirect()->to((new FirstPage())->getPageUrl())->send();
        }

        session()->put('lastStep', self::PAGE_TYPE);

        return view('registration.pages.success', ['user' => auth()->user()])->render();
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function save(Request $request): bool
    {
        return false;
    }

    /**
     * @return RenderablePageInterface|null
     */
    public function getNextPageResolver(): ?RenderablePageInterface
    {
        return null;
    }

    /**
     * @return string|null
     */
    public function getPageUrl(): ?string
    {
        return route('page.save', ['step' => self:: PAGE_TYPE]);
    }
}
