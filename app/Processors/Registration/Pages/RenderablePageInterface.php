<?php

namespace App\Processors\Registration\Pages;

use Illuminate\Http\Request;

/**
 * Interface RenderablePageInterface
 * @package App\Processors\Registration\Pages
 */
interface RenderablePageInterface
{
    /**
     * @return string
     */
    public function render(): string;

    /**
     * @param Request $request
     * @return bool
     */
    public function save(Request $request): bool;

    /**
     * @return RenderablePageInterface|null
     */
    public function getNextPageResolver(): ?RenderablePageInterface;

    /**
     * @return string|null
     */
    public function getPageUrl(): ?string;
}
