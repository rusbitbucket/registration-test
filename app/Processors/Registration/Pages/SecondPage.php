<?php

namespace App\Processors\Registration\Pages;

use Illuminate\Http\Request;

class SecondPage implements RenderablePageInterface
{
    const PAGE_TYPE = 2;

    /**
     * @return string
     */
    public function render(): string
    {
        session()->put('lastStep', self::PAGE_TYPE);
        return view('registration.pages.second', [
            'streetAddress' => auth()->user()->street_address ?? '',
            'houseNumber' => auth()->user()->house_number ?? '',
            'zipCode' => auth()->user()->zip_code ?? '',
            'city' => auth()->user()->city ?? '',
        ])->render();
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function save(Request $request): bool
    {
        if (!auth()->check()) {
            redirect()->to((new FirstPage())->getPageUrl())->send();
        }

        $request->validate([
            'street_address' => 'required|string|min:1|max:100',
            'house_number' => 'required|string|min:1|max:20',
            'zip_code' => 'required|string|min:1|max:20',
            'city' => 'required|string|min:1|max:100',
        ]);

        auth()->user()->update($request->all());

        return true;
    }

    /**
     * @return RenderablePageInterface|null
     */
    public function getNextPageResolver(): ?RenderablePageInterface
    {
        return new AccountDetailsPage();
    }

    /**
     * @return string|null
     */
    public function getPageUrl(): ?string
    {
        return route('page.save', ['step' => self:: PAGE_TYPE]);
    }
}
