<?php

namespace App\Http\Controllers;

use App\Processors\Registration\Pages\FirstPage;
use App\Processors\Registration\RegistrationProcessor;
use Illuminate\Http\RedirectResponse;

/**
 * Class MainController
 * @package App\Http\Controllers
 */
class MainController extends Controller
{
    /**
     * @return RedirectResponse
     */
    public function index(): RedirectResponse
    {
        $lastStep = session()->get('lastStep');
        if ($lastStep) {
            return redirect((new RegistrationProcessor($lastStep))->getPageUrl());
        }
        return redirect((new FirstPage())->getPageUrl());
    }
}
