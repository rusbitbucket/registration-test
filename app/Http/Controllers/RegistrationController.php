<?php

namespace App\Http\Controllers;

use App\Processors\Registration\RegistrationProcessor;
use Illuminate\Http\Request;

class RegistrationController extends Controller
{
    /**
     * @param int $step
     * @return string
     */
    public function showPage(int $step): string
    {
        return (new RegistrationProcessor($step))->render();
    }

    /**
     * @param Request $request
     * @param int $step
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function save(Request $request, int $step)
    {
        $processor = (new RegistrationProcessor($step));
        if (!$processor->save($request)) {
            return redirect()->back()->withErrors(['error' => 'Unable to save data']);
        }
        return redirect($processor->getNextPageResolver()->getPageUrl());
    }
}
