<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class User
 * @package App\Models
 */
class User extends Authenticatable
{
    /**
     * @var array
     */
    protected $fillable = [
        'firstname',
        'lastname',
        'telephone',
        'street_address',
        'house_number',
        'zip_code',
        'city',
        'account_owner',
        'iban',
        'data',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'data' => 'json',
    ];
}
