Required php: ^7.3|^8.0

## To launch the app: 
1. Create .env from .env.example
2. Create database and configure .env
3. Create tables using database-scripts.sql file located in the root
4. Run "composer install"
5. Run "php artisan key:generate"
6. Run "php artisan serve"

## Possible performance optimizations:
- We store user data on each registration process step. It's possible to make only one query after the third step by collecting user data from previous steps, e.g. store it in cache storage first. 
 
## What could be done better:
- Interface that describes pages should be splitted into 2 parts. One interface will be for retrieving data and another one will stand for storing data. In this way we will achieve "interface segregation" principle.
- Registration steps look like this now: /registration/step/1 /registration/step/2 .... It would be safer to hide step identifiers from users. Although there are conditions added to cover bad cases, there are still some imperfections present.
- It would be better to add more info to log messages.
- User experience improvements. Like JS validation and showing more messages to users.
