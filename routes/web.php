<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'App\Http\Controllers\MainController@index');
Route::get('registration/step/{step}', 'App\Http\Controllers\RegistrationController@showPage');
Route::post('registration/step/{step}', 'App\Http\Controllers\RegistrationController@save')->name('page.save');
