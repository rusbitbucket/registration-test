create table users
(
    id             bigint unsigned auto_increment
        primary key,
    firstname      varchar(100) null,
    lastname       varchar(100) null,
    telephone      varchar(40)  null,
    street_address varchar(100) null,
    house_number   varchar(20)  null,
    zip_code       varchar(20)  null,
    city           varchar(100) null,
    account_owner  varchar(100) null,
    iban           varchar(50)  null,
    data           json         null,
    created_at     timestamp    null,
    updated_at     timestamp    null
)
    collate = utf8mb4_unicode_ci;

create table migrations
(
    id        int unsigned auto_increment
        primary key,
    migration varchar(255) not null,
    batch     int          not null
)
    collate = utf8mb4_unicode_ci;

