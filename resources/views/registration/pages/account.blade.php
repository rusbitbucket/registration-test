@extends('registration.layout')

@section('content')
    <h2 class="title">Account page</h2>
    <form method="POST">
        <div class="input-group">
            <input
                class="input--style-1"
                type="text"
                placeholder="Account owner"
                name="account_owner"
                value="{{ old('account_owner') ?? $accountOwner }}"
            >
        </div>
        <div class="input-group">
            <input
                class="input--style-1"
                type="text"
                placeholder="IBAN"
                name="iban"
                value="{{ old('iban') ?? $iban }}"
            >
        </div>
        <div class="p-t-20">
            <button class="btn btn--radius btn--green" type="submit">Save</button>
        </div>
        @csrf
    </form>
@endsection
