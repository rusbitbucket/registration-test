@extends('registration.layout')

@section('content')
    <h2 class="title">First page</h2>
    <form method="POST">
        <div class="input-group">
            <input
                class="input--style-1"
                type="text"
                placeholder="First name"
                name="firstname"
                value="{{ old('firstname') ?? $firstname }}"
            >
        </div>
        <div class="input-group">
            <input
                class="input--style-1"
                type="text"
                placeholder="Last name"
                name="lastname"
                value="{{ old('lastname') ?? $lastname }}"
            >
        </div>
        <div class="input-group">
            <input
                class="input--style-1"
                type="text"
                placeholder="Telephone"
                name="telephone"
                value="{{ old('telephone') ?? $telephone }}"
            >
        </div>
        <div class="p-t-20">
            <button class="btn btn--radius btn--green" type="submit">Save</button>
        </div>
        @csrf
    </form>
@endsection
