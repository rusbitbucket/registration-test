@extends('registration.layout')

@section('content')
    <h2 class="title">Second page</h2>
    <form method="POST">
        <div class="input-group">
            <input
                class="input--style-1"
                type="text"
                placeholder="Street address"
                name="street_address"
                value="{{ old('street_address') ?? $streetAddress }}"
            >
        </div>
        <div class="input-group">
            <input
                class="input--style-1"
                type="text"
                placeholder="House number"
                name="house_number"
                value="{{ old('house_number') ?? $houseNumber }}"
            >
        </div>
        <div class="input-group">
            <input
                class="input--style-1"
                type="text"
                placeholder="Zip code"
                name="zip_code"
                value="{{ old('zip_code') ?? $zipCode }}"
            >
        </div>
        <div class="input-group">
            <input
                class="input--style-1"
                type="text"
                placeholder="City"
                name="city"
                value="{{ old('city') ?? $city }}"
            >
        </div>
        <div class="p-t-20">
            <button class="btn btn--radius btn--green" type="submit">Save</button>
        </div>
        @csrf
    </form>
@endsection
